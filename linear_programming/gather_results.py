#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2013 Santiago Iturriaga <santiago@marga>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys

def main():
    if len(sys.argv) != 2:
        print "Usage %s <sol. path>" % sys.argv[0]
        exit(-1)
        
    sol_path = sys.argv[1]
    
    SCENARIOS=(0,3,6,9,10,11,13,14,16,17,19)
    WORKLOADS=("A.u_c_hihi","A.u_c_hilo","A.u_c_lohi","A.u_c_lolo","A.u_i_hihi","A.u_i_hilo","A.u_i_lohi","A.u_i_lolo","A.u_s_hihi","A.u_s_hilo","A.u_s_lohi","A.u_s_lolo","B.u_c_hihi","B.u_c_hilo","B.u_c_lohi","B.u_c_lolo","B.u_i_hihi","B.u_i_hilo","B.u_i_lohi","B.u_i_lolo","B.u_s_hihi","B.u_s_hilo","B.u_s_lohi","B.u_s_lolo")
    
    print "Workload\t Scenario\t Makespan\t Energy"
    
    for w in range(len(WORKLOADS)):
        for s in range(len(SCENARIOS)):
            makespan_sol_file = sol_path + "/scenario." + str(SCENARIOS[s]) + ".workload." + WORKLOADS[w] + ".lp.makespan_log"
            makespan_result_raw = open(makespan_sol_file).readlines()[1].replace("\t"," ").strip(" ").split(" ")
            makespan = float(makespan_result_raw[2])

            energy_sol_file = sol_path + "/scenario." + str(SCENARIOS[s]) + ".workload." + WORKLOADS[w] + ".lp.energy_log"            
            energy_result_raw = open(energy_sol_file).readlines()[1].replace("\t"," ").strip(" ").split(" ")
            energy = float(energy_result_raw[2])
            
            print "%s\t %s\t %s\t %s" % (WORKLOADS[w], SCENARIOS[s], makespan, energy)
    
    return 0

if __name__ == '__main__':
    main()
