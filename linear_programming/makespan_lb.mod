/* variable definitions */
set TASK;
set MACHINE;

param ETC{t in TASK, m in MACHINE};
param ENERGY_MAX{m in MACHINE};
param ENERGY_IDLE{m in MACHINE};

var x{t in TASK, m in MACHINE} >= 0, integer;
var Makespan >= 0;
var Machine_ct{m in MACHINE} >= 0;

/* objective function */
minimize f : Makespan;

/* constraints */
s.t. Task_is_assigned{t in TASK}: sum{m in MACHINE}(x[t,m]) == 1;
s.t. Machine_ct_def{m in MACHINE}: Machine_ct[m] = sum{t in TASK}(x[t,m] * ETC[t,m]); 
s.t. Makespan_def{m in MACHINE}: Makespan >= Machine_ct[m];

/* data  section */
/*data;
set TASK := 1 2 3 4;
set MACHINE := 1 2;

param ETC: 1    2:=
1           1   2
2           3   4
3           2   2
4           2   1;

param ENERGY_MAX:=
1           3
2           1;

param ENERGY_IDLE:=
1           1
2           0.5;
*/

end;
