#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#
#  Copyright 2013 Santiago Iturriaga <santiago@marga>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys

def main():
    if len(sys.argv) != 3:
        print "Usage: %s <scenario> <workload>" % sys.argv[0]
        exit(-1)

    scenario_filename = sys.argv[1]
    workload_filename = sys.argv[2]

    workload_content = []
    scenario_content = []

    for line in open(scenario_filename):
        line_data_raw = line.replace("\t"," ").split(" ")
        line_data = []

        for segment in line_data_raw:
            if (len(segment.strip()) > 0):
                line_data.append(segment.strip())

        if len(line_data) > 0:
            assert(len(line_data) == 4)
            scenario_content.append((int(line_data[0]), float(line_data[1]), float(line_data[2]), float(line_data[3])))

    #print scenario_content

    dim_machines = len(scenario_content)
    #print "Machines: %s" % dim_machines

    for line in open(workload_filename):
        if len(line.strip()) > 0:
            workload_content.append(float(line.strip()))

    dim_tasks = len(workload_content) / dim_machines
    #print "Tasks: %s" % dim_tasks

    print "data;\n"

    print "set TASK := ",
    for i in range(dim_tasks):
        print i+1,
    print ";\n"

    print "set MACHINE := ",
    for i in range(dim_machines):
        print i+1,
    print ";\n"

    print "param ETC: ",
    for i in range(dim_machines):
        print "%s " % (i+1),
    print ":="
    for i in range(dim_tasks):
        print "%s " % (i+1),
        for j in range(dim_machines):
            workload_pos = i * dim_machines + j;
            ssj = scenario_content[j][1]
            cores = scenario_content[j][0]

            print "%s " % (workload_content[workload_pos] / (ssj / (cores * 1000))),

        if i == dim_tasks-1:
            print ";"
        print

    print "param ENERGY_IDLE := "
    for i in range(dim_machines):
        e_idle = scenario_content[i][2]
        print "%s %s" % (i+1, e_idle),
        
        if i == dim_machines-1:
            print ";",
        print
    print

    print "param ENERGY_MAX := "
    for i in range(dim_machines):
        e_max = scenario_content[i][3]
        print "%s %s" % (i+1, e_max),
        
        if i == dim_machines-1:
            print ";",
        print
    print
    
    print "end;"

    return 0

if __name__ == '__main__':
    main()
